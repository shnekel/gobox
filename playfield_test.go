package main

import (
	"testing"
)

func TestPlayFieldInit(t *testing.T) {
	got := PlayField{}
	got.InitField()

	if len(got.Field) != 10 && len(got.Field[0]) != 10 {
		t.Errorf("Field length should be 10 elements %v", got)
	}
}

func TestPlayFieldHasValues(t *testing.T) {
	got := PlayField{}
	got.InitField()

	for i := range got.Field {
		for j := range got.Field[i] {
			if got.Field[i][j] == 0 {
				t.Errorf("field elements must be initialized %v", got)
			}
		}
	}
}
