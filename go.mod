module gitlab.com/shnekel/gobox

go 1.15

require (
	github.com/gdamore/tcell/v2 v2.1.0
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
)
