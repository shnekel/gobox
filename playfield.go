package main

import (
	"github.com/gdamore/tcell/v2"
	"math/rand"
	"strconv"
	"time"
)

const (
	boardSize int = 10
)

type PlayField struct {
	Field  [][]tcell.Color
	Screen tcell.Screen
}

var SelectedColors = []tcell.Color{
	tcell.ColorForestGreen,
	tcell.ColorRed,
	tcell.ColorYellow,
	tcell.ColorPurple,
	tcell.ColorBlue,
}

func (p *PlayField) InitField() {
	rand.Seed(time.Now().UnixNano())
	cells := make([][]tcell.Color, boardSize)
	for i := range cells {
		cells[i] = make([]tcell.Color, boardSize)
		for j := range cells[i] {
			cells[i][j] = SelectedColors[rand.Intn(len(SelectedColors))]
		}
	}
	p.Field = cells
}

func (p *PlayField) DrawField() {
	for i := range p.Field {
		for j := range p.Field[i] {
			cellStyle := tcell.StyleDefault.Background(p.Field[i][j])
			p.Screen.SetContent(i+5, j+2, ' ', nil, cellStyle)
		}
	}
}

func (p *PlayField) DrawButtons() {
	for i, item := range SelectedColors {
		cellStyle := tcell.StyleDefault.Background(item)
		p.Screen.SetContent(5+i*2, 13, ' ', nil, cellStyle)
		cellStyle = tcell.StyleDefault.Background(tcell.ColorBlack).Foreground(tcell.ColorWhite)
		drawText(p.Screen, 5+i*2, 14, 5+i*2, 14, cellStyle, strconv.Itoa(i+1))
	}
}

func (p *PlayField) NextStep(colorNum int) {
	selectedColor := SelectedColors[colorNum-1]
	for i := range p.Field {
		for j := range p.Field[i] {
			cellStyle := tcell.StyleDefault.Background(selectedColor)
			p.Screen.SetContent(i+5, j+2, 'X', nil, cellStyle)
		}
	}
}
